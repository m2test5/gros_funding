CREATE TABLE IF NOT EXISTS `User` (
`userID` int(11) NOT NULL,
  `userFirstName` varchar(128) NOT NULL,
  `userLastName` varchar(128) NOT NULL,
  `userMail` varchar(128) NOT NULL,
  `userPhone` varchar(32) NOT NULL,
  `userPseudo` varchar(16) NOT NULL,
  `userPassword` varchar(128) NOT NULL,
  `userIsAdmin` tinyint(1) NOT NULL,
  `userIsEntrepreneur` tinyint(1) NOT NULL,
  `userIsInvestisseur` tinyint(1) NOT NULL,
  `userIsConfirmed` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

ALTER TABLE `User`
 ADD PRIMARY KEY (`userID`), ADD UNIQUE KEY `userMail` (`userMail`,`userPseudo`);

ALTER TABLE `User`
MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;