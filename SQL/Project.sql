CREATE TABLE IF NOT EXISTS `Project` (
`projectID` int(11) NOT NULL,
  `projectDeadline` date NOT NULL,
  `projectName` varchar(128) NOT NULL,
  `projectSubmitNewDesc` text NOT NULL,
  `projectIsValidated` tinyint(1) NOT NULL,
  `projectUserID_FK` int(11) NOT NULL,
  `projectPublishedDate` date NOT NULL,
  `projectCurrentDesc` text NOT NULL,
  `projectNewDescToValidate` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `Project`
 ADD PRIMARY KEY (`projectID`), ADD UNIQUE KEY `projectUserID_FK` (`projectUserID_FK`);

ALTER TABLE `Project`
MODIFY `projectID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `Project`
ADD CONSTRAINT `Project_ibfk_1` FOREIGN KEY (`projectUserID_FK`) REFERENCES `User` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;