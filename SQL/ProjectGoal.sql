CREATE TABLE IF NOT EXISTS `ProjectGoal` (
`projectGoalID` int(11) NOT NULL,
  `projectGoalDesc` text NOT NULL,
  `projectGoalSumToReach` double NOT NULL,
  `projectGoalIsReached` tinyint(1) NOT NULL,
  `projectGoalProjectID_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ProjectGoal`
 ADD PRIMARY KEY (`projectGoalID`), ADD UNIQUE KEY `projectGoalProjectID_FK` (`projectGoalProjectID_FK`);

ALTER TABLE `ProjectGoal`
MODIFY `projectGoalID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `ProjectGoal`
ADD CONSTRAINT `ProjectGoal_ibfk_1` FOREIGN KEY (`projectGoalProjectID_FK`) REFERENCES `Project` (`projectID`) ON DELETE CASCADE ON UPDATE CASCADE;