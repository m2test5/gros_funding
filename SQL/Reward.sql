CREATE TABLE IF NOT EXISTS `Reward` (
`rewardID` int(11) NOT NULL,
  `rewardAmount` double NOT NULL,
  `rewardDesc` text NOT NULL,
  `rewardProjectID_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `Reward`
 ADD PRIMARY KEY (`rewardID`), ADD UNIQUE KEY `rewardProjectID_FK` (`rewardProjectID_FK`);

ALTER TABLE `Reward`
MODIFY `rewardID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `Reward`
ADD CONSTRAINT `Reward_ibfk_1` FOREIGN KEY (`rewardProjectID_FK`) REFERENCES `Project` (`projectID`) ON DELETE CASCADE ON UPDATE CASCADE;