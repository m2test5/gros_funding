CREATE TABLE IF NOT EXISTS `Contribution` (
  `contributionUserID_PK_FK` int(11) NOT NULL,
  `contributionProjectID_PK_FK` int(11) NOT NULL,
  `contributionAmount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `Contribution`
 ADD PRIMARY KEY (`contributionUserID_PK_FK`,`contributionProjectID_PK_FK`), ADD KEY `contributionProjectID_PK_FK` (`contributionProjectID_PK_FK`);

ALTER TABLE `Contribution`
ADD CONSTRAINT `Contribution_ibfk_2` FOREIGN KEY (`contributionProjectID_PK_FK`) REFERENCES `Project` (`projectID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `Contribution_ibfk_1` FOREIGN KEY (`contributionUserID_PK_FK`) REFERENCES `User` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;
