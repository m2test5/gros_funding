<?php session_start(); ?>
<?php

if (isset($_GET["registration"]))
{
    require_once(__DIR__ . '/Source/Vendor/Controller/ControllerRegistration.php');

    if (isset($_POST['inputUsername']) && isset($_POST['inputFirstName']) && isset($_POST['inputLastName']) && isset($_POST['inputEmail'])
    && isset($_POST['inputPhoneNumber']) && isset($_POST['inputPassword']) && isset($_POST['inputPasswordConfirm']))
    {
        $controller = new \Controller\ControllerRegistration($_POST['inputUsername'],
                $_POST['inputFirstName'],
                $_POST['inputLastName'],
                $_POST['inputEmail'],
                $_POST['inputPhoneNumber'],
                $_POST['inputPassword'],
                $_POST['inputPasswordConfirm']);
        $controller->exec();
    }
    else
    {
        $controller = new \Controller\ControllerRegistration();
        $controller->exec();
    }
}
else if (isset($_GET["connection"]))
{
    require_once(__DIR__ . '/Source/Vendor/Controller/ControllerConnection.php');

    if (isset($_POST['inputUserName']) && isset($_POST['inputPassword']))
    {
        $controller = new \Controller\ControllerConnection($_POST['inputUserName'], $_POST['inputPassword']);
        $controller->exec();
    }
    else
    {
        $controller = new \Controller\ControllerConnection();
        $controller->exec();
    }
}
else if (isset($_GET["deleteProject"])){
    require_once "./Source/Vendor/Controller/controllerDeleteProject.php";
}
else if (isset($_GET["logout"]))
{
    require_once "./Source/Vendor/Controller/controllerLogOut.php";
}
else if (isset($_POST["inputMoney"]) && isset($_POST['inputProjectID']))
{
    require_once "./Source/Vendor/Controller/controllerContribution.php";
}
else if (isset($_GET["projectID"]))
{
    require_once "./Source/Vendor/Controller/controllerSeeProject.php";
}
else if (isset($_GET["admin"]))
{
    require_once "./Source/Vendor/Controller/controllerAdmin.php";
}
else if (isset($_GET["validateID"]))
{
    require_once "./Source/Vendor/Controller/controllerValidateProject.php";
}
else if (isset($_GET["myprojects"]))
{
    require_once "./Source/Vendor/Controller/controllerMyProjects.php";
}
else if (isset($_GET["newProject"]))
{
    require_once (__DIR__ . '/Source/Vendor/Controller/ControllerCreationProject.php');

    if (isset($_POST['title']) && isset($_POST['description']) && isset($_POST['deadline']) && $_POST['objective'])
    {
        $controller = new \Controller\ControllerCreationProject($_POST['title'], $_POST['description'], $_POST['deadline'], $_POST['objective']);
        $controller->exec();
    }
    else
    {
        $controller = new \Controller\ControllerCreationProject();
        $controller->exec();
    }
}
else
{
    require_once "./Source/Vendor/Controller/controllerAccueil.php";
}

?>
