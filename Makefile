.PHONY:test preprod preprod_clean preprod_push

all: test preprod
test:
	php applis/atoum.phar -d Test/Vendor/ -bf Test/.bootstrap.atoum.php

preprod:prepod_clean prepod_push

prepod_clean:
	 sshpass -p "m2test5" ssh m2test5@m2gl.deptinfo-st.univ-fcomte.fr 'cd /home/m2test5/public_html/preprod && rm -rf *'

prepod_push:
	sshpass -p "m2test5" scp -r * m2test5@m2gl.deptinfo-st.univ-fcomte.fr:/home/m2test5/public_html/preprod
