import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Connection {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    ChromeDriverManager.getInstance().setup();
    driver = new ChromeDriver();
    /*FirefoxDriverManager.getInstance().setup();
    driver = new FirefoxDriver();*/
    baseUrl = "http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test5/preprod/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testConnectionCorrect() throws Exception {
    driver.get("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test5/preprod/");
    driver.findElement(By.linkText("Sign In")).click();
    driver.findElement(By.name("inputUserName")).clear();
    driver.findElement(By.name("inputUserName")).sendKeys("jessycolonval");
    driver.findElement(By.name("inputPassword")).click();
    driver.findElement(By.name("inputPassword")).clear();
    driver.findElement(By.name("inputPassword")).sendKeys("nVvC^#8d=.:Ms]f0)lYo#n%8q$AQ");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Registration'])[1]/following::button[1]")).click();
    assertEquals("Well you are connected", driver.findElement(By.id("message_connection")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
