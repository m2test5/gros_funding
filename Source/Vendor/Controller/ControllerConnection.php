<?php

namespace Controller
{
    require_once(__DIR__ . '/../Views/Connection/viewConnection.php');
    require_once(__DIR__ . '/../Models/UserBDD.php');
    require_once(__DIR__ . '/../Models/Crypt.php');

    class ControllerConnection
    {

        private $username;
        private $password;

        private $errors;

        /**
         * ControllerConnection constructor.
         * @param $username
         * @param $password
         */
        public function __construct($username = null, $password = null)
        {
            $this->username = $username;
            $this->password = $password;
            $this->errors = array();
        }

        /**
         * @return array Tableau contenant les erreurs rencontrées
         */
        public function getErrors()
        {
            return $this->errors;
        }

        /**
         * Vérifie que les entrées du formulaire sont correctes
         */
        private function checkFormErrors()
        {
            if ($this->username === "")
            {
                $this->errors['username'] = "Username required.";
            }
            else
            {
                if (strlen($this->username) > 16)
                {
                    $this->errors['username'] = "Username too long.";
                }
            }

            if ($this->password === "")
            {
                $this->errors['password'] = "Password required.";
            }
            else
            {
                if (strlen($this->password) > 128)
                {
                    $this->errors['password'] = "Password too long.";
                }
            }
        }

        /**
         * Vérifie que le nom d'utilisateur et le mot de passe associé son correct
         */
        private function checkConnection()
        {
            $this->checkFormErrors();
            if (count($this->errors) == 0)
            {
                $userBdd = new \UserBDD();
                try
                {
                    $user = $userBdd->findUserByPseudo($this->username);
                    $crypt = new \Models\Crypt();
                    if (!$crypt->verify($this->password, $user->getShaPassword()))
                    {
                        $this->errors['connection'] = "Incorrect password or user.";
                    }
                    else
                    {
                        $_SESSION['user'] = serialize($user);
                    }
                }
                catch (\Exception $e)
                {
                    $this->errors['database'] = "Incorrect password or user.";
                }
            }
        }

        public function exec()
        {
            $view = new \viewConnection();

            if ($this->username !== null && $this->password !== null)
            {
                $this->checkConnection();

                if (count($this->errors) == 0)
                {
                    header("location: index.php");
                }
                else
                {
                    $view->show($this->errors);
                }
            }
            else
            {
                $view->show();
            }
        }
    }
}
