<?php

require_once 'Source/Vendor/Models/Database.php';

if ( !isset($_SESSION['user']) || empty($_SESSION['user']) )
{
    require_once 'Source/Vendor/Views/Accueil/accueil.php';
}
else
{
    $projectID = $_GET["deleteProject"];

    $database = new Database();
    $database->deleteProject($projectID);

    require_once 'Source/Vendor/Controller/controllerMyProjects.php';

}

?>