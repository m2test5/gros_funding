<?php

namespace Controller
{

    require_once(__DIR__ . '/../Views/Connection/viewRegistration.php');
    require_once(__DIR__ . '/../Models/User.php');
    require_once(__DIR__ . '/../Models/UserBDD.php');
    require_once(__DIR__ . '/../Models/Crypt.php');

    class ControllerRegistration
    {
        private $username;
        private $firstname;
        private $lastname;
        private $email;
        private $phone;
        private $password;
        private $confirmPassword;

        private $errors;

        /**
         * ControllerRegistration constructor.
         * @param $username
         * @param $firstname
         * @param $lastname
         * @param $email
         * @param $phone
         * @param $password
         * @param $confirmPassword
         */
        public function __construct($username = null, $firstname = null, $lastname = null, $email = null, $phone = null, $password = null, $confirmPassword = null)
        {
            $this->username = $username;
            $this->firstname = $firstname;
            $this->lastname = $lastname;
            $this->email = $email;
            $this->phone = $phone;
            $this->password = $password;
            $this->confirmPassword = $confirmPassword;
            $this->errors = array();
        }

        /**
         * @return array
         */
        public function getErrors()
        {
            return $this->errors;
        }

        public function checkFormErrors()
        {
            if (empty($this->username))
            {
                $this->errors['username'] = "Username required.";
            }
            else
            {
                if (strlen($this->username) > 16)
                {
                    $this->errors['username'] = "Username too long.";
                }
            }

            if (empty($this->firstname))
            {
                $this->errors['firstname'] = "Firstname required.";
            }
            else
            {
                if (strlen($this->firstname) > 128)
                {
                    $this->errors['firstname'] = "Firstname too long.";
                }
            }

            if (empty($this->lastname))
            {
                $this->errors['lastname'] = "Lastname required.";
            }
            else
            {
                if (strlen($this->lastname) > 128)
                {
                    $this->errors['lastname'] = "Lastname too long.";
                }
            }

            if (empty($this->email))
            {
                $this->errors['email'] = "Email required.";
            }
            else
            {
                if (!preg_match(" /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/ ", $this->email))
                {
                    $this->errors['email'] = "Email invalid.";
                }
                else
                {
                    if (strlen($this->email) > 32)
                    {
                        $this->errors['email'] = "Email too long.";
                    }
                }
            }

            if (empty($this->phone))
            {
                $this->errors['phone'] = "Required number phone.";
            }
            else
            {
                if (!preg_match("#^0[1-6789]([-. ]?[0-9]{2}){4}$#", $this->phone))
                {
                    $this->errors['phone'] = "Number phone invalid.";
                }
                else
                {
                    if (strlen($this->phone) > 32)
                    {
                        $this->errors['phone'] = "Number phone too long.";
                    }
                }
            }

            if (empty($this->password))
            {
                $this->errors['password'] = "Required password.";
            }
            else
            {
                if (strlen($this->password) > 128)
                {
                    $this->errors['password'] = "Password too long.";
                }
                else
                {
                    if (empty($this->confirmPassword))
                    {
                        $this->errors['password'] = "Confirm password required.";
                    }
                    else
                    {
                        if (strcmp($this->password, $this->confirmPassword) !== 0)
                        {
                            $this->errors['password'] = "Passwords are different.";
                        }
                    }
                }
            }
        }

        private function checkRegistration()
        {
            $this->checkFormErrors();
            if (count($this->errors) == 0)
            {
                $userBdd = new \UserBDD();
                try
                {
                    $userBdd->findUserByPseudo($this->username);
                    $this->errors['username'] = "Username already exists.";
                }
                catch (\Exception $e) {}

                try
                {
                    $userBdd->findUserByEmail($this->email);
                    $this->errors['email'] = "Adress mail already exists.";
                }
                catch (\Exception $e) {}
            }
        }

        public function exec()
        {
            $view = new \viewRegistration();

            if ($this->username !== null && $this->firstname !== null && $this->lastname !== null && $this->email !== null
            && $this->phone !== null && $this->password !== null && $this->confirmPassword !== null)
            {
                $this->checkRegistration();

                if (count($this->errors) == 0)
                {
                    $crypt = new \Models\Crypt();
                    $user = new \User($this->username, $this->firstname, $this->lastname, $this->email, $this->phone, $crypt->hash($this->password));

                    $userBdd = new \UserBDD();
                    $userBdd->save($user);

                    // TODO Envoyer un email de confirmation

                    header("location: index.php?connection");
                }
                else
                {
                    $view->show($this->errors);
                }

            }
            else
            {
                $view->show();
            }
        }
    }
}
