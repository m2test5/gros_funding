<?php

require_once 'Source/Vendor/Models/Database.php';

if ( !isset($_SESSION['user']) || empty($_SESSION['user']) )
{
    require_once 'Source/Vendor/Views/Accueil/accueil.php';
}
else
{
    $userr = unserialize($_SESSION['user']);
    if($userr->getIsAdmin() == 1)
    {
        $database = new Database();
        $idToValidate = mysqli_real_escape_string($database->getLink(), $_GET['validateID']);

        $sqlreq = "UPDATE Project SET projectIsValidated = 1 WHERE projectID = '".$idToValidate."'";

        $res = mysqli_query($database->getLink(), $sqlreq);
        require_once 'Source/Vendor/Views/Admin/adminPage.php';
    }
    else{
        require_once 'Source/Vendor/Views/Accueil/accueilLoggedIn.php';
    }
}

?>