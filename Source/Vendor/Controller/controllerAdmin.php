<?php

require_once 'Source/Vendor/Models/User.php';

if ( !isset($_SESSION['user']) || empty($_SESSION['user']) )
{
    require_once 'Source/Vendor/Views/Accueil/accueil.php';
}
else
{
    $userr = unserialize($_SESSION['user']);
    if($userr->getIsadmin() == 1)
    {
        require_once 'Source/Vendor/Views/Admin/adminPage.php';
    }
    else{
        require_once 'Source/Vendor/Views/Accueil/accueilLoggedIn.php';
    }
}

?>