<?php

if ( !isset($_SESSION['user']) || empty($_SESSION['user']) )
{
    require_once 'Source/Vendor/Views/Accueil/accueil.php';
}
else
{
    require_once 'Source/Vendor/Views/Accueil/accueilLoggedIn.php';
}
?>