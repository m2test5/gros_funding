<?php

namespace Controller
{

    require_once (__DIR__ . '/../Views/Project/viewCreationProject.php');
    require_once (__DIR__ . '/../Models/Database.php');
    require_once (__DIR__ . '/../Models/Project.php');

    class ControllerCreationProject
    {

        private $title;
        private $description;
        private $deadline;
        private $objective;

        private $errors;

        /**
         * ControllerCreationProject constructor.
         * @param $title
         * @param $description
         * @param $deadline
         * @param $objective
         */
        public function __construct($title = null, $description = null, $deadline = null, $objective = null)
        {
            $this->title = $title;
            $this->description = $description;
            $this->deadline = $deadline;
            $this->objective = $objective;
            $this->errors = array();
        }

        /**
         * @return array
         */
        public function getErrors()
        {
            return $this->errors;
        }

        public function checkFormErrors()
        {
            if (empty($this->title))
            {
                $this->errors['title'] = "Title required.";
            }
            else
            {
                if (strlen($this->title) > 128)
                {
                    $this->errors['title'] = "Title too long.";
                }
            }

            if (empty($this->description))
            {
                $this->errors['description'] = "Description required.";
            }

            if (empty($this->deadline))
            {
                $this->errors['deadline'] = "Deadline required.";
            }
            else
            {
                if (!preg_match("#^[0-9]{4}([-][0-9]{2}){2}$#", $this->deadline))
                {
                    $this->errors['deadline'] = "Deadline invalid format.";
                }
                else
                {
                    list($y, $m, $d) = explode('-', $this->deadline);
                    if (!checkdate($m, $d, $y))
                    {
                        $this->errors['deadline'] = "Deadline invalid.";
                    }
                }
            }

            if (empty($this->objective))
            {
                $this->errors['objective'] = "Objective required.";
            }
            else
            {
                if ($this->objective <= 0)
                {
                    $this->errors['objective'] = "The objective must be strictly positive.";
                }
            }
        }

        public function exec()
        {
            $view = new \viewCreationProject();
            if ($this->title !== null && $this->description !== null && $this->deadline !== null && $this->objective !== null)
            {
                $this->checkFormErrors();
                if (count($this->errors) == 0)
                {
                    $user = unserialize($_SESSION['user']);
                    $database = new \Database();

                    $project = new \Project($this->title,
                        $this->deadline,
                        date("Y-m-d"),
                        $this->description,
                        "",
                        0,
                        0,
                        $database->getUserID($user->getPseudo())
                    );
                    $project->register($database->getLink(), $_POST['objective']);

                    header("location:index.php");
                }
                else
                {
                    $view->show($this->errors);
                }
            }
            else
            {
                $view->show();
            }
        }

    }
}