
<?php

class Project
{
    private $name = "unknown";
    private $deadline = "0000-00-00";
    private $publishedDate = "0000-00-00";
    private $currentDescription = "unknow";
    private $newDescription = "unknown";
    private $is_validated = 0; /* 0 = not validated, 1 = validated */
    private $newDescToValidate = 0; /* 0 = not validated, 1 = validated */
    private $userId=0;

    public function __construct($name,
                                $deadline,
                                $publishedDate,
                                $currentDescription,
                                $newDescription,
                                $is_validated,
                                $newDescToValidate,
                                $userId)
    {
        $this->name = $name;
        $this->deadline = $deadline;
        $this->publishedDate = $publishedDate;
        $this->currentDescription = $currentDescription;
        $this->newDescription = $newDescription;
        $this->is_validated = $is_validated;
        $this->newDescToValidate = $newDescToValidate;
        $this->userId = $userId;
    }

    // Store a project in the database.
    public function register($conn, $goal)
    {
        $sql = "INSERT INTO Project (
    projectDeadline,
    projectName,
    projectIsValidated,
    projectUserID_FK,
    projectPublishedDate,
    projectCurrentDesc,
    	projectSubmitNewDesc,
    	projectNewDescToValidate) VALUES (DATE('"
            .mysqli_real_escape_string($conn, $this->deadline)."'),'"
            .mysqli_real_escape_string($conn, $this->name)."',
            0,"
            .mysqli_real_escape_string($conn, $this->userId).",DATE('"
            .mysqli_real_escape_string($conn, $this->publishedDate)."'),'"
            .mysqli_real_escape_string($conn, $this->currentDescription). "', 'test', 0)";

        $r = mysqli_query($conn, $sql) or mysqli_error($conn);

        $descr ="";
        if ($r)
        {
            $projectId = mysqli_insert_id($conn);
            $sqlCreateGoal = "INSERT INTO ProjectGoal (projectGoalProjectID_FK, projectGoalIsReached, projectGoalDesc, projectGoalSumToReach) VALUES ('".$projectId."', 0,'".mysqli_real_escape_string($conn, $descr)."', '".mysqli_real_escape_string($conn, $goal)."')";
            $ressss = mysqli_query($conn, $sqlCreateGoal) or mysqli_error($conn);

        }

    }
}