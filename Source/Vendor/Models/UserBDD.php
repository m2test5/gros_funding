<?php

require_once(__DIR__ . '/User.php');
require_once(__DIR__ . '/Crypt.php');
require_once(__DIR__ . '/Database.php');

class UserBDD
{

    /**
     * @var string Nom de la table
     */
    private $nameTable = "User";

    /**
     * @var string Nom de l'attribut ID de la table
     */
    private $attributID = "userID";

    /**
     * @var string Nom de l'attribut "FirstName" de la table
     */
    private $attributFirstName = "userFirstName";

    /**
     * @var string Nom de l'attribut "LastName" de la table
     */
    private $attributLastName = "userLastName";

    /**
     * @var string Nom de l'attribut "Mail" de la table
     */
    private $attributMail = "userMail";

    /**
     * @var string Nom de l'attribut "Phone" de la table
     */
    private $attributPhone = "userPhone";

    /**
     * @var string Nom de l'attribut "Pseudo" de la table
     */
    private $attributPseudo = "userPseudo";

    /**
     * @var string Nom de l'attribut "Password" de la table
     */
    private $attributPassword = "userPassword";

    /**
     * @var string Nom de l'attribut "IsAdmin" de la table
     */
    private $attributIsAdmin = "userIsAdmin";

    /**
     * @var string Nom de l'attribut "IsEntrepreneur" de la table
     */
    private $attributIsEntrepreneur = "userIsEntrepreneur";

    /**
     * @var string Nom de l'attribut "IsInvestisseur" de la table
     */
    private $attributIsInvestisseur = "userIsInvestisseur";

    /**
     * @var string Nom de l'attribut "IsConfirmed" de la table
     */
    private $attributIsConfirmed = "userIsConfirmed";

    /**
     * @var Database Instance d'objet permettant de se connecter/déconnecter à la base de données
     */
    private $database;

    /**
     * UserBDD constructeur qui va instancier l'objet Database.
     */
    public function __construct()
    {
        $this->database = new Database();
    }

    /**
     * Méthode permettant d'enregistrer un utilisateur dans la base de données
     *
     * @param User $user Objet Utilisateur qu'on souhaite enregistrer
     */
    public function save(User $user)
    {
        $sql = "INSERT INTO " . $this->nameTable . "(" . $this->attributFirstName . ","
                                                       . $this->attributLastName . ","
                                                       . $this->attributMail . ","
                                                       . $this->attributPhone . ","
                                                       . $this->attributPseudo . ","
                                                       . $this->attributPassword . ","
                                                       . $this->attributIsAdmin . ","
                                                       . $this->attributIsEntrepreneur . ","
                                                       . $this->attributIsInvestisseur . ","
                                                       . $this->attributIsConfirmed .
                                                   ")" .
               "VALUES(" . "'" . $user->getFirstName() . "',"
                         . "'" . $user->getLastName() . "',"
                         . "'" . $user->getMail() . "',"
                         . "'" . $user->getPhone() . "',"
                         . "'" . $user->getPseudo() . "',"
                         . "'" . $user->getShaPassword() . "',"
                         . $user->getIsAdmin() . ","
                         . $user->getIsEntrepreneur() . ","
                         . $user->getIsInvestisseur() . ","
                         . $user->getisConfirmed() .
                     ")";

        $r = mysqli_query($this->database->getLink(), $sql) or mysqli_error($this->database->getLink());
    }

    /**
     * Méthode permettant de mettre à jour les données d'un utilisateur dans la base de données
     *
     * @param $id L'identifiant de l'utilisateur
     * @param User $user Les nouvelles données de l'utilisateur
     */
    public function update($id, User $user)
    {
        $sql = "UPDATE " . $this->nameTable . " " .
               "SET " . $this->attributFirstName . "='" . $user->getFirstName() . "',"
                      . $this->attributLastName . "='" . $user->getLastName() . "',"
                      . $this->attributMail . "='" . $user->getMail() . "',"
                      . $this->attributPhone . "='" . $user->getPhone() . "',"
                      . $this->attributPseudo . "='" . $user->getPseudo() . "',"
                      . $this->attributPassword . "='" . $user->getShaPassword() . "',"
                      . $this->attributIsAdmin . "=" . $user->getIsAdmin() . ","
                      . $this->attributIsEntrepreneur . "=" . $user->getIsEntrepreneur() . ","
                      . $this->attributIsInvestisseur . "=" . $user->getIsInvestisseur() . ","
                      . $this->attributIsConfirmed . "=" . $user->getisConfirmed() . " " .
               "WHERE " . $this->attributID . "=" . $id;

        $r = mysqli_query($this->database->getLink(), $sql) or mysqli_error($this->database->getLink());
    }

    /**
     * Méthode permettant de recupérer les données d'un utilisateur grâce à son identifiant
     *
     * @param $id L'identifiant de l'utilisateur
     * @return User L'objet contenant les informations de l'utilisateur correspondant à l'identifiant
     * @throws Exception Si aucun résultat est trouvé
     */
    public function findUser($id)
    {
        $sql = "SELECT * " .
               "FROM " . $this->nameTable . " " .
               "WHERE " . $this->attributID . "=" . $id;
        $r = mysqli_query($this->database->getLink(), $sql) or mysqli_error($this->database->getLink());

        if ($r->num_rows == 0)
        {
            throw new Exception("No results found !");
        }

        $enr = mysqli_fetch_assoc($r);

        return new User($enr[$this->attributPseudo],
            $enr[$this->attributFirstName],
            $enr[$this->attributLastName],
            $enr[$this->attributMail],
            $enr[$this->attributPhone],
            $enr[$this->attributPassword],
            $enr[$this->attributIsAdmin],
            $enr[$this->attributIsEntrepreneur],
            $enr[$this->attributIsInvestisseur],
            $enr[$this->attributIsConfirmed]);
    }

    public function findUserByPseudo($pseudo)
    {
        $sql = "SELECT * " .
               "FROM " . $this->nameTable . " " .
               "WHERE " . $this->attributPseudo . "='" . $pseudo . "'";
        $r = mysqli_query($this->database->getLink(), $sql) or mysqli_error($this->database->getLink());

        if ($r->num_rows == 0)
        {
            throw new Exception("No results found !");
        }

        $enr = mysqli_fetch_assoc($r);
        return new User($enr[$this->attributPseudo],
            $enr[$this->attributFirstName],
            $enr[$this->attributLastName],
            $enr[$this->attributMail],
            $enr[$this->attributPhone],
            $enr[$this->attributPassword],
            $enr[$this->attributIsAdmin],
            $enr[$this->attributIsEntrepreneur],
            $enr[$this->attributIsInvestisseur],
            $enr[$this->attributIsConfirmed]);
    }

    public function findUserByEmail($email)
    {
        $sql = "SELECT * " .
            "FROM " . $this->nameTable . " " .
            "WHERE " . $this->attributMail . "='" . $email . "'";
        $r = mysqli_query($this->database->getLink(), $sql) or mysqli_error($this->database->getLink());

        if ($r->num_rows == 0)
        {
            throw new Exception("No results found !");
        }

        $enr = mysqli_fetch_assoc($r);
        return new User($enr[$this->attributPseudo],
            $enr[$this->attributFirstName],
            $enr[$this->attributLastName],
            $enr[$this->attributMail],
            $enr[$this->attributPhone],
            $enr[$this->attributPassword],
            $enr[$this->attributIsAdmin],
            $enr[$this->attributIsEntrepreneur],
            $enr[$this->attributIsInvestisseur],
            $enr[$this->attributIsConfirmed]);
    }
}