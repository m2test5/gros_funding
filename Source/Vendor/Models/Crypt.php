<?php

namespace Models {

    class Crypt
    {
        private $cost;

        public function __construct()
        {
            $timeTarget = 0.05;

            $this->cost = 8;
            do {
                $this->cost++;
                $start = microtime(true);
                password_hash("test", CRYPT_SHA256, ["cost" => $this->cost]);
                $end = microtime(true);
            } while (($end - $start) < $timeTarget);
        }

        public function hash($var)
        {
            return password_hash($var, CRYPT_SHA256, ["cost" => $this->cost]);
        }

        public function verify($password, $hash)
        {
            return password_verify($password, $hash);
        }
    }

}