<?php

class User
{
    private $pseudo = "unknown";
    private $firstName = "unknown";
    private $lastName = "unknown";
    private $mail = "unknown";
    private $phone = "0000000000";
    private $shaPassword = "";
    private $isAdmin = 0; /* 0 = not admin, 1 = admin */
    private $isEntrepreneur = 0; /* 0 = not entrepreneur, 1 = entrepreneur */
    private $isInvestisseur = 0; /* 0 = not investisseur, 1 = investisseur */
    private $isConfirmed = 1;

    /**
     * User constructor.
     * @param string $pseudo
     * @param string $firstName
     * @param string $lastName
     * @param string $mail
     * @param string $phone
     * @param string $password
     * @param int $isAdmin
     * @param int $isEntrepreneur
     * @param int $isInvestisseur
     * @param int $isConfirmed
     */
    public function __construct($pseudo, $firstName, $lastName, $mail, $phone, $password, $isAdmin = 0, $isEntrepreneur = 0, $isInvestisseur = 0, $isConfirmed = 1)
    {
        $this->pseudo = $pseudo;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->mail = $mail;
        $this->phone = $phone;
        $this->shaPassword = $password;
        $this->isAdmin = $isAdmin;
        $this->isEntrepreneur = $isEntrepreneur;
        $this->isInvestisseur = $isInvestisseur;
        $this->isConfirmed = $isConfirmed;
    }

    /**
     * @return string
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param string $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getShaPassword()
    {
        return $this->shaPassword;
    }

    /**
     * @param string $shaPassword
     */
    public function setShaPassword($shaPassword)
    {
        $this->shaPassword = $shaPassword;
    }

    /**
     * @return int
     */
    public function getisAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @param int $isAdmin
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;
    }

    /**
     * @return int
     */
    public function getisEntrepreneur()
    {
        return $this->isEntrepreneur;
    }

    /**
     * @param int $isEntrepreneur
     */
    public function setIsEntrepreneur($isEntrepreneur)
    {
        $this->isEntrepreneur = $isEntrepreneur;
    }

    /**
     * @return int
     */
    public function getisInvestisseur()
    {
        return $this->isInvestisseur;
    }

    /**
     * @param int $isInvestisseur
     */
    public function setIsInvestisseur($isInvestisseur)
    {
        $this->isInvestisseur = $isInvestisseur;
    }

    /**
     * @return int
     */
    public function getisConfirmed()
    {
        return $this->isConfirmed;
    }

    /**
     * @param int $isConfirmed
     */
    public function setIsConfirmed($isConfirmed)
    {
        $this->isConfirmed = $isConfirmed;
    }
}