<?php
require_once 'Source/Vendor/Models/Crypt.php';
require_once 'Source/Vendor/Models/User.php';

class Database
{

    private $host;
    private $user;
    private $password;
    private $nameBDD;

    private $link;

    public function __construct()
    {
        // Connexion wamp local jessy
        //$this->host = 'localhost';
        /*$this->user = 'root';
        $this->password = '';
        $this->nameBDD = 'gros_funding';*/


       /*PILOU BDD LOCAL
        $this->host = 'localhost';
        $this->user = 'root';
        $this->password = '';
        $this->nameBDD = 'm2test5';*/

        $this->host = "m2gl";
        $this->user = "m2test5";
        $this->password = "m2test5";
        $this->nameBDD = "m2test5";

        $this->connection();
    }

    public function __destruct()
    {
        $this->deconnection();
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    private function connection()
    {
        $this->link = new mysqli($this->host, $this->user, $this->password, $this->nameBDD);
        $this->link->set_charset('utf8');
    }

    private function deconnection()
    {
        mysqli_close($this->link);
    }

    public function getUserID($username)
    {
        $sql = "SELECT userID FROM User WHERE userPseudo='".$username."'";
        $result = mysqli_query($this->link, $sql);


        if ($result->num_rows == 1)
        {
            $row = mysqli_fetch_assoc($result);
            return $row['userID'];
        }

    }

    public function deleteProject($projectID){
        $sql = "DELETE FROM Project WHERE projectID ='".$projectID."'";
        $res = mysqli_query($this->link, $sql) or mysqli_error($this->link);

    }
}