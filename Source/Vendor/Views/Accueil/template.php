<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="./Source/Vendor/Style/logo.ico">

        <!-- Bootstrap CSS -->
        <link href="./Source/Vendor/Style/bootstrap.min.css" rel="stylesheet">

        <title><?= $title ?></title>
    </head>

    <body>
        <?= $content ?>

        <script src="./Source/Vendor/Script/jquery-3.3.1.slim.min.js"></script>
        <script src="./Source/Vendor/Script/bootstrap.min.js"></script>
        <script src="./Source/Vendor/Script/popper.min.js"></script>
    </body>

</html>
