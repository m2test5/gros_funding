<?php $title = "Welcome"; ?>

<?php ob_start(); ?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">Gros Funding</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php?connection">Sign In</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?registration">Sign Up</a>
                </li>
            </ul>
        </div>
    </nav>

    <h1>Welcome to GROS FUNDING</h1>
<?php $content = ob_get_clean(); ?>

<?php require_once 'template.php' ?>
