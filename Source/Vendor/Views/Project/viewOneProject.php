<?php
require_once "Source/Vendor/Models/User.php";
require_once "Source/Vendor/Models/Database.php";
$title = "Welcome";


function show_nav()
{
    ob_start();
    ?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">Gros Funding</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php?myprojects">My projects</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?igaveto">Project I gros funded</a>
                </li>
                <?php
                $objuser = unserialize($_SESSION['user']);
                if ($objuser->getIsAdmin() == 1) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?admin">Admin page</a>
                    </li>
                    <?php
                }
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?logout">Log out</a>
                </li>
            </ul>
        </div>
    </nav>

    <?php
}

function show_project()
{
    $database = new Database();

    $usr = unserialize($_SESSION['user']);
    $pseudo = mysqli_real_escape_string($database->getLink(), $usr->getPseudo());
    $reqID = "SELECT userID FROM User WHERE userPseudo = '".$pseudo."'";

    $resID = mysqli_query($database->getLink(), $reqID);

    $resFinalID = mysqli_fetch_assoc($resID)['userID'];

    $reqS = "SELECT * FROM ProjectGoal, Project WHERE projectGoalProjectID_FK = projectID AND projectID = '".$_GET["projectID"]."'";


    $res = mysqli_query($database->getLink(), $reqS);


    $reqAmountGiven = "SELECT SUM(contributionAmount) AS sumContrib FROM Contribution WHERE contributionProjectID_PK_FK =  '".$_GET["projectID"]."'";
    $res2 = mysqli_query($database->getLink(), $reqAmountGiven);

    ob_start();

    ?>

    <?php
    $row2 = mysqli_fetch_assoc($res2);

    while($roww = mysqli_fetch_assoc($res)) {
        ?>
        <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="jumbotron" id="<?= $roww['projectID'] ?>">
                    <h2><?= $roww['projectName'] ?> </h2>
                    <div class="row">
                        <div class="col-sm-8">
                            <p class="text-justify"> <?= $roww['projectCurrentDesc'] ?></p></div>
                        <div class="col-sm-2">Goal is : $<?= $roww['projectGoalSumToReach'] ?>
                            Current funds given : $<?= $row2['sumContrib'] ?>
                        </div>
                        <div class="col-sm-2"> Available until <?= $roww['projectDeadline'] ?>
                            <a role="button" class="btn btn-primary"
                               href="index.php?projectID=<?= $roww['projectID'] ?>">Edit</a>
                            <?php
                            if ($usr->getisAdmin() == 1) {
                                ?>
                                <a role="button" class="btn btn-danger"
                                   href="index.php?deleteProject=<?= $roww['projectID'] ?>">Delete</a>
                                <?php
                            }


                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <form class="form-contribution" method="post">
                                <h1 class="h3 mb-3 font-weight-normal">Give money :</h1>

                                <label for="inputMoney" class="sr-only">Amount to give</label>
                                <input type="number" name="inputMoney" class="form-control" placeholder="I give...."
                                       required autofocus>
                                <input type="hidden" name="inputProjectID" value="<?= $roww['projectID'] ?>">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Give !</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }

}


show_nav();

show_project();

$content = ob_get_clean();
require 'template.php';