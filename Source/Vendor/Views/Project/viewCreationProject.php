<?php
/**
 * Created by PhpStorm.
 * User: slbz
 * Date: 16/10/18
 * Time: 16:12
 */



class viewCreationProject
{
    function show_nav()
    {
        ob_start();
        ?>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="index.php">Gros Funding</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?myprojects">My projects</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?igaveto">Project I gros funded</a>
                    </li>
                    <?php
                    if (isset($_SESSION['user']) && !empty($_SESSION['user']))
                    {
                        $objuser = unserialize($_SESSION['user']);
                        if ($objuser->getIsAdmin() == 1) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?admin">Admin page</a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?logout">Log out</a>
                    </li>
                </ul>
            </div>
        </nav>

        <?php
    }

    public function show($errors = array())
    {
        $this->show_nav();
        ob_start();

        ?>
        <form  class="form-signin" method="post">
            <h1 class="h3 mb-3 font-weight-normal">Submit a project</h1>

            <label for="title" class="sr-only">Title</label>
            <input type="text" name="title" class="form-control" placeholder="Title" required autofocus id="title"/>



            <label for="description" class="sr-only">Description</label>
            <textarea name="description" class="form-control" rows="10" cols="30" id="description" required>
            Project's description.
        </textarea>



            <label for="deadline" class="sr-only">Deadline</label>
            <input type="date" name="deadline" class="form-control" id="deadline" required/>



            <label for="objective" class="sr-only">Objective</label>
            <input type="number" name="objective" min="1" max="99999999999" class="form-control" id="objective" required/>


            <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
        </form>


        <?php

        if ( !empty($errors) )
        {
            echo "<h1>Errors</h1>";

            echo "<ul>";

            foreach ($errors as $error)
            {
                echo "<li>".$error."</li>";
            }

            echo "</ul>";
        }

        $title = "Project";
        $content = ob_get_clean();

        require_once "template.php";
    }
}
