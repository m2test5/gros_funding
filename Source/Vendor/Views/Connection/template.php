<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="./Source/Vendor/Style/logo.ico">

        <title><?= $title ?></title>

        <!-- Bootstrap core CSS -->
        <link href="./Source/Vendor/Style/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="./Source/Vendor/Style/signin.css" rel="stylesheet">
    </head>

    <body class="text-center">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">Gros Funding</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php?connection">Sign In</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?registration">Sign Up</a>
                </li>
            </ul>
        </div>
    </nav>
        <?= $content ?>
    </body>

</html>
