<?php

class viewRegistration
 {
     public function show($errors = array())
     {
        ob_start();

        ?>
        <form class="form-signin" method="post">
            <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

            <?php
            if ( !empty($errors) )
            {
                foreach ($errors as $error)
                {
                    echo "<p style='color: red;'>" . $error . "</p>";
                }
            }
            ?>

            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" name="inputEmail" class="form-control" placeholder="Email address" required autofocus>

            <label for="inputUsername" class="sr-only">Username</label>
            <input type="text" name="inputUsername" class="form-control mb-2" placeholder="Username" required>

            <div class="form-row">
                <div class="col">
                    <label for="inputFirstName" class="sr-only">Firstname</label>
                    <input type="text" name="inputFirstName" class="form-control" placeholder="Firstname" required>
                </div>
                <div class="col">
                    <label for="inputLastName" class="sr-only">Lastname</label>
                    <input type="text" name="inputLastName" class="form-control" placeholder="Lastname" required>
                </div>
            </div>

            <label for="inputPhoneNumber" class="sr-only">Phone number</label>
            <input type="text" name="inputPhoneNumber" class="form-control mb-2" placeholder="Phone number" required>

            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="inputPassword" class="form-control mb-0" placeholder="Password" required>

            <label for="inputPasswordConfirm" class="sr-only">Confirm password</label>
            <input type="password" name="inputPasswordConfirm" class="form-control" placeholder="Confirm password" required>

            <div class="checkbox mb-3">
                <label>
                    Already have an account ? <a href="index.php?connection">Log in</a>
                </label>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
        </form>

         <?php

         $title = "Registration";
         $content = ob_get_clean();

         require_once "template.php";
    }
}


