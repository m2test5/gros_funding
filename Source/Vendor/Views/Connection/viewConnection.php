<?php

class viewConnection
{
    public function show($errors = array())
    {
        ob_start();

        ?>
        <form class="form-signin" method="post">
            <h1 class="h3 mb-3 font-weight-normal">Please log in</h1>

            <?php
            if ( !empty($errors) )
            {
                foreach ($errors as $error)
                {
                    echo "<p style='color: red;'>" . $error . "</p>";
                }
            }
            ?>

            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="text" name="inputUserName" class="form-control" placeholder="Username" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="inputPassword" class="form-control" placeholder="Password" required>
            <div class="checkbox mb-3">
                <label>
                    No account ? <a href="index.php?registration">Registration</a>
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
        </form>

        <?php
        $title = "Connection";
        $content = ob_get_clean();

        require_once "template.php";
    }
}
