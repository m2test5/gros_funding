<?php
require_once "Source/Vendor/Models/User.php";
require_once "Source/Vendor/Models/Database.php";
$title = "Welcome";


function show_nav()
{
    ob_start();
    ?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">Gros Funding</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php?myprojects">My projects</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?igaveto">Project I gros funded</a>
                </li>
                <?php
                $objuser = unserialize($_SESSION['user']);
                if ($objuser->getIsAdmin() == 1) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?admin">Admin page</a>
                    </li>
                    <?php
                }
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?logout">Log out</a>
                </li>
            </ul>
        </div>
    </nav>

    <?php
}

function show_projects_toValidate()
{
    $database = new Database();
    $req = "SELECT * FROM ProjectGoal, Project WHERE projectGoalProjectID_FK = projectID AND projectGoalIsReached = 0 AND projectIsValidated = 0 ORDER BY projectPublishedDate ASC";

    $res = mysqli_query($database->getLink(), $req);
    ob_start();

    ?>
    <h1>Projects to validate</h1>
    <?php
    while ($row = mysqli_fetch_assoc($res))
    {

        ?>
        <div class = "container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="jumbotron">
                        <h2><?= $row['projectName'] ?> </h2>
                        <div class="row">
                            <div class="col-sm-8">
                                <p class="text-justify"> <?= $row['projectCurrentDesc'] ?></p></div>
                            <div class="col-sm-2">Goal is : $<?= $row['projectGoalSumToReach'] ?></div>
                            <div class="col-sm-2"> Available until <?= $row['projectDeadline'] ?>
                                <a role="button" class="btn btn-success" href="index.php?validateID=<?=$row['projectID']?>">Validate</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }

}


show_nav();

show_projects_toValidate();

$content = ob_get_clean();
require 'template.php';
