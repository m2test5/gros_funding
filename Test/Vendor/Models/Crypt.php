<?php

namespace Models\tests\units;

use \atoum;

class Crypt extends atoum
{

    public function testCompare()
    {
        $var1 = "Test";
        $this
            ->if($crypt = new \Models\Crypt())
            ->then
                ->boolean($crypt->verify($var1, $crypt->hash($var1)));
    }
}