<?php

namespace Controller\tests\units;

use \atoum;

class ControllerRegistration extends atoum
{

    public function testFromCorrect()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "0678894556", "aPassword", "aPassword"))
            ->and($controller->checkFormErrors())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(0)
        ;

        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "0778894556", "aPassword", "aPassword"))
            ->and($controller->checkFormErrors())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(0)
        ;

        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "0878894556", "aPassword", "aPassword"))
            ->and($controller->checkFormErrors())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(0)
        ;

        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "0978894556", "aPassword", "aPassword"))
            ->and($controller->checkFormErrors())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(0)
        ;
    }

    public function testFormEmptyUsername()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("", "aFirstname", "aLastname", "aEmail@gmail.com", "0666666666", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['username']->isEqualTo("Username required.")
        ;
    }

    public function testFormUsernameTooLong()
    {
        $usernameTooLong = "";
        for ($i = 0; $i < 17; $i++)
        {
            $usernameTooLong .= "a";
        }

        $this
            ->if($controller = new \Controller\ControllerRegistration($usernameTooLong, "aFirstname", "aLastname", "aEmail@gmail.com", "0666666666", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
            ->array($errors)
            ->hasSize(1)
            ->string['username']->isEqualTo("Username too long.")
        ;
    }

    public function testFormEnptyFirstname()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "", "aLastname", "aEmail@gmail.com", "0666666666", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
            ->array($errors)
            ->hasSize(1)
            ->string['firstname']->isEqualTo("Firstname required.")
        ;
    }

    public function testFormFirstnameTooLong()
    {
        $firstnameTooLong = "";
        for ($i = 0; $i < 129; $i++)
        {
            $firstnameTooLong .= "a";
        }

        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", $firstnameTooLong, "aLastname", "aEmail@gmail.com", "0666666666", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
            ->array($errors)
            ->hasSize(1)
            ->string['firstname']->isEqualTo("Firstname too long.")
        ;
    }

    public function testFormEmptyLastname()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "", "aEmail@gmail.com", "0666666666", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['lastname']->isEqualTo("Lastname required.")
        ;
    }

    public function testFormLastnameTooLong()
    {
        $lastnameTooLong = "";
        for ($i = 0; $i < 129; $i++)
        {
            $lastnameTooLong .= "a";
        }

        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", $lastnameTooLong, "aEmail@gmail.com", "0666666666", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['lastname']->isEqualTo("Lastname too long.")
        ;
    }

    public function testFormEmailEmpty()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "", "0666666666", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['email']->isEqualTo("Email required.")
        ;
    }

    public function testFormEmailInvalid()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail", "0666666666", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['email']->isEqualTo("Email invalid.")
        ;
    }

    public function testFormEmailTooLong()
    {
        $emailTooLong = "";
        for ($i = 0; $i < 32; $i++)
        {
            $emailTooLong .= "a";
        }
        $emailTooLong .= "@gmail.com";

        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", $emailTooLong, "0666666666", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['email']->isEqualTo("Email too long.")
        ;
    }

    public function testFormPhoneEmpty()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['phone']->isEqualTo("Required number phone.")
        ;
    }

    public function testFormPhoneInvalid()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "123456", "aPassword", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['phone']->isEqualTo("Number phone invalid.")
        ;
    }

    public function testFormPasswordEmpty()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "0666666666", "", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['password']->isEqualTo("Required password.")
        ;
    }

    public function testFormPasswordTooLong()
    {
        $passwordTooLong = "";
        for ($i = 0; $i < 129; $i++)
        {
            $passwordTooLong .= "a";
        }

        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "0666666666", $passwordTooLong, "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['password']->isEqualTo("Password too long.")
        ;
    }

    public function testFormConfirmEmpty()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "0666666666", "aPassword", ""))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['password']->isEqualTo("Confirm password required.")
        ;
    }

    public function testFormPasswordDifferentConfirm()
    {
        $this
            ->if($controller = new \Controller\ControllerRegistration("aUsername", "aFirstname", "aLastname", "aEmail@gmail.com", "0666666666", "aPassword", "aPassword1"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['password']->isEqualTo("Passwords are different.")
        ;
    }
}