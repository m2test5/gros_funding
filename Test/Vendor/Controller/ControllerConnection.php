<?php

namespace Controller\tests\units;

use \atoum;

class ControllerConnection extends atoum
{

    public function testFormEmptyUsername()
    {
        $this
            ->if($controller = new \Controller\ControllerConnection("", "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['username']->isEqualTo("Username required.")
        ;
    }

    public function testFormUsernameTooLong()
    {
        $usernameTooLong = "";
        for ($i = 0; $i < 17; $i++)
        {
            $usernameTooLong .= "a";
        }

        $this
            ->if($controller = new \Controller\ControllerConnection($usernameTooLong, "aPassword"))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['username']->isEqualTo("Username too long.")
        ;
    }

    public function testFormEmptyPassword()
    {
        $this
            ->if($controller = new \Controller\ControllerConnection("aUserName", ""))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['password']->isEqualTo("Password required.")
        ;
    }

    public function testFormPasswordTooLong()
    {
        $passwordTooLong = "";
        for ($i = 0; $i < 129; $i++)
        {
            $passwordTooLong .= "a";
        }

        $this
            ->if($controller = new \Controller\ControllerConnection("aUserName", $passwordTooLong))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['password']->isEqualTo("Password too long.")
        ;
    }

    public function testFormEmpty()
    {
        $this
            ->if($controller = new \Controller\ControllerConnection("", ""))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(2)
                ->string['username']->isEqualTo("Username required.")
                ->string['password']->isEqualTo("Password required.")
        ;
    }

    public function testFormTooLong()
    {
        $usernameTooLong = "";
        for ($i = 0; $i < 17; $i++)
        {
            $usernameTooLong .= "a";
        }

        $passwordTooLong = "";
        for ($i = 0; $i < 129; $i++)
        {
            $passwordTooLong .= "a";
        }

        $this
            ->if($controller = new \Controller\ControllerConnection($usernameTooLong, $passwordTooLong))
            ->and($controller->exec())
            ->and($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(2)
                ->string['username']->isEqualTo("Username too long.")
                ->string['password']->isEqualTo("Password too long.")
        ;
    }
}