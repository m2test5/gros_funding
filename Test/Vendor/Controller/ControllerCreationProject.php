<?php

namespace Controller\tests\units;

use \atoum;

class ControllerCreationProject extends atoum
{

    public function testFormEmptyTitle()
    {
        $this
            ->if ($controller = new \Controller\ControllerCreationProject("", "Description", "2020-02-20", "9999"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['title']->isEqualTo("Title required.")
        ;
    }

    public function testFormTitleTooLong()
    {
        $titleTooLong = "";
        for ($i = 0; $i < 129; ++$i)
        {
            $titleTooLong .= "a";
        }

        $this
            ->if ($controller = new \Controller\ControllerCreationProject($titleTooLong, "Description", "2020-02-20", "9999"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['title']->isEqualTo("Title too long.")
        ;
    }

    public function testFormEmptyDescription()
    {
        $this
            ->if ($controller = new \Controller\ControllerCreationProject("Title", "", "2020-02-20", "9999"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['description']->isEqualTo("Description required.")
        ;
    }

    public function testFormEmptyDeadline()
    {
        $this
            ->if ($controller = new \Controller\ControllerCreationProject("Title", "Description", "", "9999"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['deadline']->isEqualTo("Deadline required.")
        ;
    }

    public function testFormDeadlineInvalidFormat()
    {
        $this
            ->if ($controller = new \Controller\ControllerCreationProject("Title", "Description", "09-30-2018", "9999"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['deadline']->isEqualTo("Deadline invalid format.")
        ;

        $this
            ->if ($controller = new \Controller\ControllerCreationProject("Title", "Description", "date", "9999"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['deadline']->isEqualTo("Deadline invalid format.")
        ;
    }

    public function testFormDeadlineInvalid()
    {
        $this
            ->if ($controller = new \Controller\ControllerCreationProject("Title", "Description", "2018-09-31", "9999"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['deadline']->isEqualTo("Deadline invalid.")
        ;

        $this
            ->if ($controller = new \Controller\ControllerCreationProject("Title", "Description", "2020-02-30", "9999"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['deadline']->isEqualTo("Deadline invalid.")
        ;
    }

    public function testFormEmptyObjective()
    {
        $this
            ->if ($controller = new \Controller\ControllerCreationProject("Title", "Description", "2018-09-30", ""))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['objective']->isEqualTo("Objective required.")
        ;
    }

    public function testFormObjectiveBadInterval()
    {
        $this
            ->if ($controller = new \Controller\ControllerCreationProject("Title", "Description", "2018-09-30", "-1"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['objective']->isEqualTo("The objective must be strictly positive.")
        ;

        $this
            ->if ($controller = new \Controller\ControllerCreationProject("Title", "Description", "2018-09-30", "0"))
            ->and ($controller->checkFormErrors())
            ->and ($errors = $controller->getErrors())
            ->then
                ->array($errors)
                ->hasSize(1)
                ->string['objective']->isEqualTo("Objective required.")
        ;
    }

}